Rutas REST en Express

Ahora pondremos en práctica lo que hemos visto en lecciones anteriores. 
Crearemos una carpeta llamada routes donde colocaremos las rutas de nuestra
aplicación. Otra forma como lo hacen es que combinan las rutas con el 
controlador mismo pero este no será el caso.

Una instancia Router es un sistema de middleware y direccionamiento completo;
por este motivo, a menudo se conoce como una “miniaplicación”.