Comunicar el servidor Http y el servidor de Websockets

¿Cómo vamos a comunicar nuestra aplicación con nuestro servidor de Websockets
a traves del servidor de websockets mismo?
Lo que haremos es convertir nuestro servidor HTTP en un cliente más de 
Websockets.

Así como la aplicación web es un cliente, ahora el servidor web va a ser otro
cliente y eso nos enseña que no solo desde el Front-end nos podemos conectar a
un servidor de Websockets sino también desde otras aplicaciones desde el 
Back-end. Para ello debemos instalar:

npm install socket.io-client

Este es el mismo que usa el Front-end que importamos en la lección pasada pero
lo estamos instalando como un paquete para poder utilizarlo desde nodejs mismo.

Ahora lo separamos creando una carpeta llamada realtime/ y crearemos un archivo
llamado client.js. Desde ahí definiremos la lógica del cliente que se va a 
comunicar hacia el servidor.

Esto:
const io = require('socket.io-client');
Es similar a esto:
socket = io.connect()

Una vez todo configurado, debemos importarlo después que se haya ejecutado la
librería socket.io. Fijemonos que apenas al arrancar el servidor, el 
automáticamente se conecta al servidor e imprime el mensaje que habíamos 
establecido en esta lección.

Podemos hacer otra prueba. Cuando entremos desde la vista y si tenemos tan solo
un navegador, este va a indicar que hay dos personas conectadas. Una es el 
back-end y el otro es el front-end. De esta forma es como vamos a conectar
nuestro servidor Http con nuestro servidor de Websockets.