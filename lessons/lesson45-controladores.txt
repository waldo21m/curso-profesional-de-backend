Controladores

Es el intermediario entre la vista y el modelo. Generalmente un controlador 
recibe una petición http, consulta al modelo en caso de que sea necesario
y hace el render a la vista con los datos que extrajo del modelo.

El código que hemos visto en lecciones anteriores en el archivo de server.js
(como app.get o app.post por ejemplo) es el que vamos a trabajar en los
controladores. Es común que cada modelo tenga su propio controlador y
precisamente un controlador solo corresponda a un modelo, sin embargo, esta
no es una regla escrita y en ocasiones habrá controladores que no empaten con
ningún modelo, tal como puede haber modelos sin un controlador correspondiente.
En nodejs la convención es que el controlador sea llamado exactamente igual
que el modelo pero en plural (en esta lección crearemos en task.js en la carpeta
controllers).

Cuando nuestra aplicación se separa en múltiples archivos recurrimos a la
naturaleza modular de javascript para compartir información entre todos estos
archivos. Las aplicaciones escritas con JS son modulares. Un archivo exporta
piezas de funcionalidad como clases y funciones y otro archivo las importa.
Esto abre el abanico de posibilidades para que exista una variedad amplia de
soluciones o formas en que podamos estructurar nuestro módulo. Para este proyecto,
exportaremos objetos JSON en el controlador, estos objetos contendrán funciones
que manejarán las peticiones http que vengan y estén relacionadas con nuestro
controlador. Pero nosotros podemos se capaces también de exportar clases, 
funciones, entre otros.

Cuando importamos una carpeta como lo tenemos en la siguiente instrucción:

const Task = require('../models').Task;

Lo que hace en realidad es buscar un archivo index.js y exporta es simplemente
lo que indica este archivo. En este caso si exploramos el archivo index.js de
la carpeta models podemos observar que está exportando es lo siguiente:

module.exports = db;

Como es db, nosotros solo necesitaríamos es el modelo que vayamos a necesitar.
Desde el archivo index.js si queremos usar un modelo basta con escribir

db.Task
db.User
etc

Pero cuando importamos es por eso que colocamos .Task y es para hacer referencia
de que vamos a usar solo este modelo y no todo el objeto db.