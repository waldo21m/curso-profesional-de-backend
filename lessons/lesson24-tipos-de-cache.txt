Tipos de caché

De acuerdo con la Mozilla Developer Network, en general podemos identificar dos
tipos de cache, los locales y los compartidos.

Prácticamente todos los usuarios tienen un caché local que viene incluido por
defecto con su navegador, este puede servir para que las páginas que visitamos
carguen más rápido o bien para que podemos ver un sitio incluso cuando no tengamos
internet.

Este es un buen ejemplo de un caché local, lo que lo diferencía de un caché
compartido es que este caché sólo sirve para un usuario, aquél que accede al
sitio web vía dicha computadora.

Por otro lado, un caché compartido puede estar compartido entre múltiples usuarios,
existen servicios que ofrecen la posibilidad de funcionar como intermediarios
entre los usuarios y el servidor, un buen ejemplo de ello es Cloudflare.

Los cachés compartido son bastante interesantes porque pueden ofrecer beneficios
de performance y velocidad incluso a usuarios que visitan por primera vez nuestro
sitio web. Algunos de estos servicios replican las copias que se han generado en
múltiples ubicaciones del mundo, acercándolas más a los usuarios que
eventualmente requerirán estas copias.

Continuemos.