Middlewares para protección de rutas

Desafortunadamente en express.js, los middlewares se aplican para todo porque
van en una misma pila o un mismo stack. Es por ello que dentro del middleware
debemos preguntarle a que rutas es que aplicará dicho middleware.