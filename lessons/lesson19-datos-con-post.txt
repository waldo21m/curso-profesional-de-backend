Datos con POST

Para recibir datos con el método POST en Express debemos usar body-parser. 
Body-parser extrae toda la parte del cuerpo de un flujo de solicitud entrante y
lo expone en req.body como algo con lo que es más fácil interactuar. Si También
puede hacerlo usted mismo, pero el uso del analizador corporal hará lo que sea
necesario y le ahorrará problemas. Para instalar la dependencia ejecutamos:

npm install body-parser

Una vez realizado, importamos y lo configuramos (pronto se explicará mejor estos
pasos) y procedemos a hacer la prueba en Postman usando el verbo http POST
indicando la ruta y usando la pestaña Body con la opción x-www-form-urlencoded.
En key ingresamos el nombre del campo y en value el valor que va a recibir.
En este campo, a diferencia el método GET si permite recibir espacios en blanco.