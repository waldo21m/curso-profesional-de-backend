Relaciones uno a muchos - El esquema

Para hacer variaciones en nuestro esquema de migraciones podemos crear una
migración que se encargue de agregar la llave foránea a la tabla 
correspondiente. Ejemplo:

sequelize migration:create --name add_user_id_to_tasks

Es importante saber que todos los métodos de queryInterface retornan una
promesa. Una vez realizado todos nuestros cambios ejecutamos el comando:

sequelize db:migrate