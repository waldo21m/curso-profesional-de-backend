'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Tasks', 'userId', {
      type: Sequelize.INTEGER,
      references: {
        model: {
          tableName: 'Users' // De esta forma, específicamos a que tabla hace la referencia y no a un módelo específico
        },
        key: 'id' // Nombre del campo de la tabla users con el que se hace la relación
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Tasks', 'userId');
  }
};
