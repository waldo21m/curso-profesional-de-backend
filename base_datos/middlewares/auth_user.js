module.exports = function (req, res, next) {
    if (! (req.originalUrl.toLowerCase().includes("tasks") || req.originalUrl.toLowerCase().includes("categories"))) return next();

    if (req.session.userId) return next();

    res.redirect('/sessions');
}