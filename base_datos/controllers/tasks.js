const Task = require('../models').Task; // Así importamos modelos en sequelize
const User = require('../models').User;

module.exports = {
    index: function (req, res) {
        Task.findAll().then((tasks => {
            res.render('tasks/index', { tasks: req.user.tasks }); // Con req.user.tasks solo mostramos las tareas del usuario logueado
        }));
    },

    create: function (req, res) {
        Task.create({
            description: req.body.description,
            userId: req.user.id
        }).then(result => {
            let categories = req.body.categories.split(',');
            result.setCategories(categories);
            res.json(result);
        }).catch(err => {
            console.log(err);
            res.json(err);
        });
    },

    new: function (req, res) {
        res.render('tasks/new');
    },

    show: function (req, res) {
        Task.findByPk(req.params.id, {
            include: [
                {
                    model: User,
                    as: 'user'
                },
                'categories'
            ]
        }).then(task => {
            res.render('tasks/show', { task }); // Shorthand property
        });
    },

    edit: function (req, res) {
        Task.findByPk(req.params.id).then(task => {
            res.render('tasks/edit', { task }); // Shorthand property
        });
    },

    update: function (req, res) {
        Task.update(
            { description: req.body.description },
            {
                where: {
                    id: req.params.id
                }
            }).then(response => {
                res.redirect('/tasks/' + req.params.id);
            })
    },

    destroy: function (req, res) {
        Task.destroy({
            where: {
                id: req.params.id
            }
        }).then(contadorElementosEliminados => {
            res.redirect('/tasks');
        });
    }
};