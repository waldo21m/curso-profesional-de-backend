const express = require('express');
const cookieSession = require('cookie-session');

const app = express();

app.use(cookieSession({
    name: 'session', // El valor por defecto de name es session
    keys: ['bvjkdsbjvkbsdjkvbsdkbvksd', 'hfkdwnjvksdbjvbsdjvbjsd'] // Va el valor que queramos en diferentes strings y son usados para firmar las cookies mismas.
}));

app.get('/', function (req, res) {
    req.session.visits = req.session.visits || 0; // req.session.visits al cargar la primera vez es undefined y de ser así, agarrará el valor 0
    req.session.visits = req.session.visits + 1;

    res.send(`${req.session.visits} visita(s)`);
});

app.listen(3000);